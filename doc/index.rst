.. Sixteen Pawns documentation master file, created by
   sphinx-quickstart on Sat May 13 18:38:58 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sixteen Pawns's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   models
   game_engine
   test
   legal_jumps
   legal_moves
   consumers
   views
   urls


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
