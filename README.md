![16paws_board_picture.JPG](https://bitbucket.org/repo/KrMzrMr/images/4255070161-16paws_board_picture.JPG)![16paws_board_picture.JPG](https://bitbucket.org/repo/KrMzrMr/images/1564563939-16paws_board_picture.JPG)# README #

We are going to build up a simple 2d puzzle game using phaser.js.
Name of this game is "16 paws".
Originally this game is played in rural bangladesh and india.

Rules:
Rules of this game are,

-> 2 opponents.
-> 16 paws each. white and black, red or black or any other color.
-> Paws can go anywhere within line only one step.
-> To take out opponent's paw, your paw have to go over the opponent's paw (like checkers).
-> 1 paw can take out several paws at once if have the opportunity. (like checkers)
-> Paw can move to any direction. (where in checkers paws can go only forward, unless its a queen)
-> If scope comes you can skip not to take out opponent's paw. (where in checkers you have to take out)
-> Same pawn can not played more then 3 times.
-> Same move can not be played more then 3 times.



Let's get it started.
Thank you!