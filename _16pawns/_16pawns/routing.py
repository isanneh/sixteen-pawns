from channels.routing import route
from SixteenPawns.consumers import ws_add, ws_message, ws_disconnect

channel_routing = [
    route("websocket.connect", ws_add),
    route("websocket.receive", ws_message),
    #route("websocket.connect", "SixteenPawns.consumers.ws_add", path=r'^/game/(?P<game_id>\w+)$'),
    route("websocket.disconnect", ws_disconnect),
]
