# -*- coding: utf-8 -*-
""" models.py """
from __future__ import unicode_literals
from datetime import datetime
from django.db import models
from jsonfield import JSONField
from django.contrib.auth.models import User

GAME_STATUS = (
    ('M', 'MatchingPlayers'),
    ('IP', 'InProgress'),
    ('GO', 'GameOver'),
    ('T', 'Terminated')
)

# Create your models here.
class Game(models.Model):
    """This model is the back bone for a game in the system as it requires 2
    players, board_data in the JSON (dictionary) format, winner, scores,
    current player's turn, and date."""
    player1 = models.ForeignKey(User, related_name='game1')
    player2 = models.ForeignKey(User, related_name='game2', null=True, blank=True)
    board_data = JSONField()
    status = models.CharField(default='M', max_length=2, choices=GAME_STATUS)
    winner = models.ForeignKey(User, related_name='games_won', null=True, blank=True)
    player1_score = models.IntegerField(default=0)
    player2_score = models.IntegerField(default=0)
    current_player = models.ForeignKey(User, related_name='turn', null=True, blank=True)
    date = models.DateField(default=datetime.now())


class OnlineUser(models.Model):
    """This model is used to keep track of online users"""
    user = models.ForeignKey(User, related_name='online')
    is_online = models.BooleanField(default=False)
    date = models.DateField(default=datetime.now())
