#import json
'''sed to convert python dictionary into JSON format'''
#import copy
from SixteenPawns.legal_moves import is_move_legal
from SixteenPawns.legal_jumps import legal_jump
from SixteenPawns.models import Game


def findIdInBoard(board, x, y):
    """Looks through board array and find a pawn at the location (x,y),
    if not found returns -1, else it will return the Id of the pawn."""
    for pawn in board:
        if pawn['x'] == x and pawn['y'] == y:
            return pawn['id']
    return -1

def removePawn(board, id):
    """Remove a pawn from the board array given its id."""
    board[:] = [p for p in board if p['id'] != id]
    return board

def movePawn(board, id, new_x, new_y):
    """Changes the (x,y) coordinates of the pawn with the given id to (new_x,new_y).
	This function does not do any validation or checks as to whether or not
	the move is legal, it is a simple set function."""
	#Assume that the new_x and new_y are ok and that pawn with id is still in board
    for pawn in board:
        if pawn['id'] == id:
            pawn['x'] = new_x
            pawn['y'] = new_y
    return board


def print_board(board):
    """Prints the board in a friendly format."""
    print "#       The Board Map"
    print "#    1   2   3   4   5"
    print "#    -----------------"
    for y in range(1, 10):
        print "#   ",
        for x in range(1, 6):
            id = findIdInBoard(board, x, y)
            if id != -1:
                if id < 10:
                    print id, " ",
                else:
                    print id, "",
            else:
                print "   ",
        print ""



def validLocation(x, y, init):
    """
	Takes in an x,y pair and whether or not it its used to initialize the board.
	It returns True if the location is valid and False otherwise.
	If "init" == True, only (x,y) pairs that are initial positions would return
	true, otherwise if "init" ==False, then other positions (i.e Row 5) is
	included.
	"""

    if x == 2 and y == 1:
        return False
    elif x == 4 and y == 1:
        return False
    elif x == 1 and y == 2:
        return False
    elif x == 5 and y == 2:
        return False
    elif x == 1 and y == 5  and init == True:
        return False
    elif x == 2 and y == 5 and init == True:
        return False
    elif x == 3 and y == 5 and init == True:
        return False
    elif x == 4 and y == 5 and init == True:
        return False
    elif x == 5 and y == 5 and init == True:
        return False
    elif x == 1 and y == 8:
        return False
    elif x == 5 and y == 8:
        return False
    elif x == 2 and y == 9:
        return False
    elif x == 4 and y == 9:
        return False
    else:
        return True

#init_board is responsible for returning a new board with pawns in initial position
def init_board():
    """
	Returns a board with pawns in the initial positions.
	"""
    board = [] #Empty board
    id_counter = 1
    for y in range(1, 10):
        for x in range(1, 6):
            if validLocation(x, y, True):
                board.append({"id" : id_counter, "x" : x, "y" : y})
                id_counter = id_counter + 1
    return board

def processMove(game, user, old_x, old_y, new_x, new_y):
    """
		This is the heart of the engine. It takes a Game object, user
		(who made the move), old location and new location and
		determines whether or not the move is valid. If valid, it returns True,
		the updated board, and pawns removed (if any).
    """
    board = game.board_data
    if old_x == new_x and old_y == new_y:
	    #No change
        print "no change"
        return False, board, "Invalid move- you moved the pawn to the same location", 0

    pawn_id = findIdInBoard(board, old_x, old_y)

    if findIdInBoard(board, new_x, new_y) != -1:
        print "not empty"
            # check if position pawn is be moved is not occupied
        return False, board, "Invalid move- there is a pawn in this location", 0

    elif pawn_id <= 16 and user != game.player1:
        print pawn_id
        return False, board, "The pawn you are trying to move does not belong to you", 0

    elif pawn_id > 16 and user != game.player2:
        return False, board, "The pawn you are trying to move does not belong to you", 0

    else:

        if is_move_legal(old_y, old_x, new_y, new_x):
            print "legal"
			#Let's grab the id from the board
            return True, movePawn(board, pawn_id, new_x, new_y), "", 0
            print "moves", old_y, old_x, new_y, new_x
    pawns_to_remove = legal_jump(old_y, old_x, new_y, new_x)
    print pawns_to_remove
    pawns_removed = len(pawns_to_remove)
    if pawns_removed == 0:
        print "no remove"
			#There is nothing to remove
        print "THere is nothing to remove"

        return False, board, "Invalid move", 0

    else:
        for pawn in pawns_to_remove:
            pawn_id = findIdInBoard(board, int(pawn[1]), int(pawn[0]))
            removedPawnBoard = removePawn(board, pawn_id)

			#move the pawn
            pawn_id = findIdInBoard(board, old_x, old_y)
            return True, movePawn(board, pawn_id, new_x, new_y), "", pawns_removed

def handleMoveRequest(gameId, user, old_x, old_y, new_x, new_y):
    """
	Called from the view, this function is merely a helper function to call the
	processMove function and send the output back to the client.
    """

    game = Game.objects.get(pk=gameId)
        # deepcopy is used to create a new object
        # because python uses object by reference
        #new_board = copy.deepcopy(game.board_data)

    print_board(game.board_data)

	#player check, which player is supposed to move

	#if the board is ok, call process move
    is_legal, new_board, error_message, pawns_collected = \
        processMove(game, user, old_x, old_y, new_x, new_y)

    return is_legal, new_board, error_message, pawns_collected
#board = [] #Empty board
#board = init_board()
#print_board(board)
#print json.dumps(board)
#print
#print "Going to Remove pawn Id: 1"
#print
#print_board(removePawn(board,1))
#print
#print "Going to move pawn with id 2 to (3,5)"
#print
#print_board(movePawn(board,2,3,5))
