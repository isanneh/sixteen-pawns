# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-02 17:34
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SixteenPawns', '0005_auto_20170502_0131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='date',
            field=models.DateField(default=datetime.datetime(2017, 5, 2, 17, 34, 27, 609252)),
        ),
    ]
