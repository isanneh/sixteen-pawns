# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-05 01:17
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SixteenPawns', '0008_auto_20170502_2245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='date',
            field=models.DateField(default=datetime.datetime(2017, 5, 5, 1, 17, 33, 534543)),
        ),
        migrations.AlterField(
            model_name='game',
            name='status',
            field=models.CharField(choices=[('M', 'MatchingPlayers'), ('IP', 'InProgress'), ('GO', 'GameOver'), ('T', 'Terminated')], default='M', max_length=2),
        ),
    ]
