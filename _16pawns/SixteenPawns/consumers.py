""" consumers.py """
import json
from channels import Group
from channels.sessions import channel_session
from channels.auth import channel_session_user, channel_session_user_from_http
from channels.sessions import channel_session
from SixteenPawns.models import Game, OnlineUser
from SixteenPawns.game_engine import handleMoveRequest
from django.contrib.sessions.models import Session
from django.utils import timezone

# Connected to websocket.connect
@channel_session
@channel_session_user_from_http
def ws_add(message):
    '''Accept the connection, add to the chat group, print game_id.'''
    # Accept the connection
    message.reply_channel.send({"accept": True})
    # add user to the online group
    Group("online").add(message.reply_channel)
    print "connection", message.user, message.content
    # Add to the chat group
    

    if "welcome" in message.content["path"]:
        
        user, is_created = OnlineUser.objects.get_or_create(user=message.user)
        user.is_online = True
        user.save()

        Group("online").add(message.reply_channel)
    else:
        game_group = message.content["path"].replace("/", "")
        Group(game_group).add(message.reply_channel)
        user_chat = "%s_%s" %(message.user.id, game_group)
        Group(user_chat).add(message.reply_channel)

# Connected to websocket.receive
@channel_session
@channel_session_user
def ws_message(message):
    '''connected to the websocket.receive'''
    print message.content
    data = json.loads(message.content['text'])

    if data["status"] == "welcome":
        data["user"] = message.user.first_name
        online_users = get_online_users()
        users = []
        
        for o in online_users:
            user = {
                'name': o.user.first_name,
                'id': o.user.id
            }
            users.append(user)
        
        data["online_users"] = users
        Group("online").send({
            "text": json.dumps(data),
        })
        return

    elif data["status"] == "chat":
        data["user"] = message.user.first_name
        Group("online").send({
            "text": json.dumps(data),
        })
        return


    game_group = message.content["path"].replace("/", "")
    game = Game.objects.get(pk=data["game_id"])
    data["user"] = message.user.first_name
    user_chat = "%s_%s" %(message.user.id, game_group)
    player1_first_name = game.player1.first_name
    if game.player2:
        player2_first_name = game.player2.first_name
    else:
        player2_first_name = ""

    if game.status == "GO":
        data["alert_user"] = "This game is over. %s won the game." % game.winner.first_name
        data["alert_all"] = data["alert_user"]
        Group(user_chat).send({
            "text": json.dumps(data),
        })
        return

    elif data["state"] == "join":

        if message.user == game.player1 and game.player2 is not None:
            data["alert_user"] = "%s has joined the game" % message.user.first_name
            Group("%s_%s" %(game.player2.id, game_group)).send({
                "text": json.dumps(data),
            })
        elif message.user == game.player2 and game.player1 is not None:
            data["alert_user"] = "%s has joined the game" % message.user.first_name
            Group("%s_%s" %(game.player1.id, game_group)).send({
                "text": json.dumps(data),
            })

        data["alert_user"] = ""

    elif data["state"] == "move" and game.status != "M":
        if game.current_player != message.user:
            data["is_move_legal"] = False
            data["alert_user"] = "%s, you have to wait for \
                your turn to make a move" % message.user.first_name
            Group(user_chat).send({
                "text": json.dumps(data),
            })
            return

        elif game.status == "GO":
            if game.winner == game.player1:
                data["alert_user"] = "You cannot make a move because \
                    the game is over. %s won the game" % player1_first_name
            elif game.winner == game.player2:
                data["alert_user"] = "You cannot make a move because \
                    the game is over. %s won the game" % player2_first_name
            else:
                data["alert_user"] = "You cannot make a move because the game is over."
            data["alert_all"] = data["alert_user"]
            Group(user_chat).send({
                "text": json.dumps(data),
            })
            return

        is_legal, board, error_message, pawns_collected = handleMoveRequest(
            data["game_id"],
            message.user,
            data["old_x"],
            data["old_y"],
            data["new_x"],
            data["new_y"]
        )

        data["board"] = board
        data["is_move_legal"] = False
        data["player1_score"] = game.player1_score
        data["player2_score"] = game.player2_score
        if is_legal:
            '''check if the move is legal'''
            # move is legal
            data["is_move_legal"] = True
            game.board_data = board
            if message.user == game.player1:
                game.player1_score = game.player1_score + pawns_collected
                game.current_player = game.player2
                data["player1_score"] = game.player1_score
                data["current_player_id"] = game.player2.id
                #data["player"] = 2
            else:
                game.player2_score = game.player2_score + pawns_collected
                game.current_player = game.player1
                data["player2_score"] = game.player2_score
                data["current_player_id"] = game.player1.id
                #data["player"] = 1

            if game.player1_score == 16:
                game.status = "GO"
                game.winner = game.player1
                data["alert_user"] = "%s has won the game" % player1_first_name
                data["alert_all"] = data["alert_user"]
            elif game.player2_score == 16:
                game.status = "GO"
                game.winner = game.player2
                data["alert_user"] = "%s has won the game" % player2_first_name
                data["alert_all"] = data["alert_user"]
            elif game.current_player == game.player1:
                data["alert_all"] = "It is %s's turn to make a move" % player1_first_name
            else:
                data["alert_all"] = "It is %s's turn to make a move" % player2_first_name



            game.save()

            Group(game_group).send({
                "text": json.dumps(data),
            })
            return

        else:
            data["alert_user"] = error_message
            Group(user_chat).send({
                "text": json.dumps(data),
            })
            return

    if game.status == "M":
        data["alert_all"] = "Waiting for second player to join"
        data["alert_user"] = "Waiting for second player to join"
    elif game.current_player == game.player1:
        data["alert_all"] = "It is %s's turn to make a move" % player1_first_name
    else:
        data["alert_all"] = "It is %s's turn to make a move" % player2_first_name

    Group(game_group).send({
        "text": json.dumps(data),
    })

# Connected to websocket.disconnect
@channel_session
@channel_session_user
def ws_disconnect(message):
    '''connceted to the websocket disconnect'''
    #if "game" in message.content.path:
    #    game_group = message.content["path"].replace("/", "")
    #    user_chat = "%s_%s" %(message.user.id, game_group)
    #    Group(user_chat).discard(message.reply_channel)
    #Group("online").discard(message.reply_channel)
    
    user, created = OnlineUser.objects.get_or_create(user=message.user)
    user.is_online = False
    user.save()



def get_online_users():
    """ get online users """
    return OnlineUser.objects.filter(is_online=True).all()
