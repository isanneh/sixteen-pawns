# -*- coding: utf-8 -*-
""" unit tests """
from __future__ import unicode_literals
from django.test import TestCase, RequestFactory
from SixteenPawns.game_engine import *
from SixteenPawns.models import Game
from SixteenPawns import views
from SixteenPawns.legal_moves import is_move_legal
from SixteenPawns.legal_jumps import legal_jump, get_legal_jumps
from django.contrib.auth.models import AnonymousUser, User
from channels import Group
from channels.test import ChannelTestCase, HttpClient
from channels import Channel


# Create your tests here.


class GameEngine(TestCase):
    ''' Starting of multple tests'''
    
    def setUp(self):
        """ setup """
        self.user1 = User.objects.create_user(
            username='John', first_name="John",
            email='john@abc.com', password='pass'
        )
        self.user2 = User.objects.create_user(
            username='Jane', first_name="Jane",
            email='jane@abc.com',
            password='pass'
        )

    def test_init_board(self):
        '''Test the board initialization'''
        board_init = [
            {'y': 1, 'x': 1, 'id': 1},
            {'y': 1, 'x': 3, 'id': 2},
            {'y': 1, 'x': 5, 'id': 3},
            {'y': 2, 'x': 2, 'id': 4},
            {'y': 2, 'x': 3, 'id': 5},
            {'y': 2, 'x': 4, 'id': 6},
            {'y': 3, 'x': 1, 'id': 7},
            {'y': 3, 'x': 2, 'id': 8},
            {'y': 3, 'x': 3, 'id': 9},
            {'y': 3, 'x': 4, 'id': 10},
            {'y': 3, 'x': 5, 'id': 11},
            {'y': 4, 'x': 1, 'id': 12},
            {'y': 4, 'x': 2, 'id': 13},
            {'y': 4, 'x': 3, 'id': 14},
            {'y': 4, 'x': 4, 'id': 15},
            {'y': 4, 'x': 5, 'id': 16},
            {'y': 6, 'x': 1, 'id': 17},
            {'y': 6, 'x': 2, 'id': 18},
            {'y': 6, 'x': 3, 'id': 19},
            {'y': 6, 'x': 4, 'id': 20},
            {'y': 6, 'x': 5, 'id': 21},
            {'y': 7, 'x': 1, 'id': 22},
            {'y': 7, 'x': 2, 'id': 23},
            {'y': 7, 'x': 3, 'id': 24},
            {'y': 7, 'x': 4, 'id': 25},
            {'y': 7, 'x': 5, 'id': 26},
            {'y': 8, 'x': 2, 'id': 27},
            {'y': 8, 'x': 3, 'id': 28},
            {'y': 8, 'x': 4, 'id': 29},
            {'y': 9, 'x': 1, 'id': 30},
            {'y': 9, 'x': 3, 'id': 31},
            {'y': 9, 'x': 5, 'id': 32}
            ]

        self.assertEqual(init_board(), board_init)

	def test_validLocation(self):
		'''Test the each valid pawns location'''

		self.assertEqual(validLocation(1, 1, True), True)
		self.assertEqual(validLocation(2, 1, True), False)
		self.assertEqual(validLocation(3, 1, True), True)
		self.assertEqual(validLocation(4, 1, True), False)
		self.assertEqual(validLocation(5, 1, True), True)
		self.assertEqual(validLocation(1, 2, True), False)
		self.assertEqual(validLocation(2, 2, True), True)
		self.assertEqual(validLocation(3, 2, True), True)
		self.assertEqual(validLocation(4, 2, True), True)
		self.assertEqual(validLocation(5, 2, True), False)
		self.assertEqual(validLocation(1, 3, True), True)
		self.assertEqual(validLocation(2, 3, True), True)
		self.assertEqual(validLocation(3, 3, True), True)
		self.assertEqual(validLocation(4, 3, True), True)
		self.assertEqual(validLocation(5, 3, True), True)
		self.assertEqual(validLocation(1, 4, True), True)
		self.assertEqual(validLocation(2, 4, True), True)
		self.assertEqual(validLocation(3, 4, True), True)
		self.assertEqual(validLocation(4, 4, True), True)
		self.assertEqual(validLocation(5, 4, True), True)
		self.assertEqual(validLocation(1, 5, True), False)
		self.assertEqual(validLocation(2, 5, True), False)
		self.assertEqual(validLocation(3, 5, True), False)
		self.assertEqual(validLocation(4, 5, True), False)
		self.assertEqual(validLocation(5, 5, True), False)
		self.assertEqual(validLocation(1, 6, True), True)
		self.assertEqual(validLocation(2, 6, True), True)
		self.assertEqual(validLocation(3, 6, True), True)
		self.assertEqual(validLocation(4, 6, True), True)
		self.assertEqual(validLocation(5, 6, True), True)
		self.assertEqual(validLocation(1, 7, True), True)
		self.assertEqual(validLocation(2, 7, True), True)
		self.assertEqual(validLocation(3, 7, True), True)
		self.assertEqual(validLocation(4, 7, True), True)
		self.assertEqual(validLocation(5, 7, True), True)
		self.assertEqual(validLocation(1, 8, True), False)
		self.assertEqual(validLocation(2, 8, True), True)
		self.assertEqual(validLocation(3, 8, True), True)
		self.assertEqual(validLocation(4, 8, True), True)
		self.assertEqual(validLocation(5, 8, True), False)
		self.assertEqual(validLocation(1, 9, True), True)
		self.assertEqual(validLocation(2, 9, True), False)
		self.assertEqual(validLocation(3, 9, True), True)
		self.assertEqual(validLocation(4, 9, True), False)
		self.assertEqual(validLocation(5, 9, True), True)

		'''Test rest of positions that are not initial postions
		Test initial positions first'''
		self.assertEqual(validLocation(1, 1, False), True)
		self.assertEqual(validLocation(2, 1, False), False)
		self.assertEqual(validLocation(3, 1, False), True)
		self.assertEqual(validLocation(4, 1, False), False)
		self.assertEqual(validLocation(5, 1, False), True)
		self.assertEqual(validLocation(1, 2, False), False)
		self.assertEqual(validLocation(2, 2, False), True)
		self.assertEqual(validLocation(3, 2, False), True)
		self.assertEqual(validLocation(4, 2, False), True)
		self.assertEqual(validLocation(5, 2, False), False)
		self.assertEqual(validLocation(1, 3, False), True)
		self.assertEqual(validLocation(2, 3, False), True)
		self.assertEqual(validLocation(3, 3, False), True)
		self.assertEqual(validLocation(4, 3, False), True)
		self.assertEqual(validLocation(5, 3, False), True)
		self.assertEqual(validLocation(1, 4, False), True)
		self.assertEqual(validLocation(2, 4, False), True)
		self.assertEqual(validLocation(3, 4, False), True)
		self.assertEqual(validLocation(4, 4, False), True)
		self.assertEqual(validLocation(5, 4, False), True)
		self.assertEqual(validLocation(1, 5, False), True)
		self.assertEqual(validLocation(2, 5, False), True)
		self.assertEqual(validLocation(3, 5, False), True)
		self.assertEqual(validLocation(4, 5, False), True)
		self.assertEqual(validLocation(5, 5, False), True)
		self.assertEqual(validLocation(1, 6, False), True)
		self.assertEqual(validLocation(2, 6, False), True)
		self.assertEqual(validLocation(3, 6, False), True)
		self.assertEqual(validLocation(4, 6, False), True)
		self.assertEqual(validLocation(5, 6, False), True)
		self.assertEqual(validLocation(1, 7, False), True)
		self.assertEqual(validLocation(2, 7, False), True)
		self.assertEqual(validLocation(3, 7, False), True)
		self.assertEqual(validLocation(4, 7, False), True)
		self.assertEqual(validLocation(5, 7, False), True)
		self.assertEqual(validLocation(1, 8, False), False)
		self.assertEqual(validLocation(2, 8, False), True)
		self.assertEqual(validLocation(3, 8, False), True)
		self.assertEqual(validLocation(4, 8, False), True)
		self.assertEqual(validLocation(5, 8, False), False)
		self.assertEqual(validLocation(1, 9, False), True)
		self.assertEqual(validLocation(2, 9, False), False)
		self.assertEqual(validLocation(3, 9, False), True)
		self.assertEqual(validLocation(4, 9, False), False)
		self.assertEqual(validLocation(5, 9, False), True)

	def test_findIdInBoard(self):
		'''begin testing that id in initial board is okay'''

		board = init_board()
		id_counter = 1
		for y in range(1, 10):
			for x in range(1, 6):
				if(validLocation(x, y, True)):
					self.assertEqual(findIdInBoard(board, x, y), id_counter)
					id_counter = id_counter + 1

		'''now we test varying id's in 1 valid location'''
		for i in range(1, 32):
			self.assertEqual(findIdInBoard([{'x':1, 'y':1, 'id':i}], 1, 1), i)

	def test_removePawn(self):
		'''test the oppponent pawn removal'''
		board = init_board()

		board_removed = [
			                 {'y': 1, 'x': 3, 'id': 2},
			                 {'y': 1, 'x': 5, 'id': 3},
			                 {'y': 2, 'x': 2, 'id': 4},
			                 {'y': 2, 'x': 3, 'id': 5},
			                 {'y': 2, 'x': 4, 'id': 6},
			                 {'y': 3, 'x': 1, 'id': 7},
			                 {'y': 3, 'x': 2, 'id': 8},
			                 {'y': 3, 'x': 3, 'id': 9},
			                 {'y': 3, 'x': 4, 'id': 10},
			                 {'y': 3, 'x': 5, 'id': 11},
			                 {'y': 4, 'x': 1, 'id': 12},
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 1), board_removed)
		'''do it again to assure no change if you try to remove a pawn that is not there'''
		self.assertEqual(removePawn(board, 1), board_removed)

		'''now remove pawns in different places until no more'''
		board_removed = [
			                 {'y': 1, 'x': 5, 'id': 3},
			                 {'y': 2, 'x': 2, 'id': 4},
			                 {'y': 2, 'x': 3, 'id': 5},
			                 {'y': 2, 'x': 4, 'id': 6},
			                 {'y': 3, 'x': 1, 'id': 7},
			                 {'y': 3, 'x': 2, 'id': 8},
			                 {'y': 3, 'x': 3, 'id': 9},
			                 {'y': 3, 'x': 4, 'id': 10},
			                 {'y': 3, 'x': 5, 'id': 11},
			                 {'y': 4, 'x': 1, 'id': 12},
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 2), board_removed)

		board_removed = [
			                 {'y': 2, 'x': 2, 'id': 4},
			                 {'y': 2, 'x': 3, 'id': 5},
			                 {'y': 2, 'x': 4, 'id': 6},
			                 {'y': 3, 'x': 1, 'id': 7},
			                 {'y': 3, 'x': 2, 'id': 8},
			                 {'y': 3, 'x': 3, 'id': 9},
			                 {'y': 3, 'x': 4, 'id': 10},
			                 {'y': 3, 'x': 5, 'id': 11},
			                 {'y': 4, 'x': 1, 'id': 12},
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 3), board_removed)

		board_removed = [
			                 {'y': 2, 'x': 3, 'id': 5},
			                 {'y': 2, 'x': 4, 'id': 6},
			                 {'y': 3, 'x': 1, 'id': 7},
			                 {'y': 3, 'x': 2, 'id': 8},
			                 {'y': 3, 'x': 3, 'id': 9},
			                 {'y': 3, 'x': 4, 'id': 10},
			                 {'y': 3, 'x': 5, 'id': 11},
			                 {'y': 4, 'x': 1, 'id': 12},
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 4), board_removed)

		board_removed = [
			                 {'y': 2, 'x': 4, 'id': 6},
			                 {'y': 3, 'x': 1, 'id': 7},
			                 {'y': 3, 'x': 2, 'id': 8},
			                 {'y': 3, 'x': 3, 'id': 9},
			                 {'y': 3, 'x': 4, 'id': 10},
			                 {'y': 3, 'x': 5, 'id': 11},
			                 {'y': 4, 'x': 1, 'id': 12},
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 5), board_removed)

		board_removed = [
			                 {'y': 3, 'x': 1, 'id': 7},
			                 {'y': 3, 'x': 2, 'id': 8},
			                 {'y': 3, 'x': 3, 'id': 9},
			                 {'y': 3, 'x': 4, 'id': 10},
			                 {'y': 3, 'x': 5, 'id': 11},
			                 {'y': 4, 'x': 1, 'id': 12},
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 6), board_removed)

		board_removed = [
			                 {'y': 3, 'x': 2, 'id': 8},
			                 {'y': 3, 'x': 3, 'id': 9},
			                 {'y': 3, 'x': 4, 'id': 10},
			                 {'y': 3, 'x': 5, 'id': 11},
			                 {'y': 4, 'x': 1, 'id': 12},
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 7), board_removed)

		board_removed = [
			                 {'y': 3, 'x': 3, 'id': 9},
			                 {'y': 3, 'x': 4, 'id': 10},
			                 {'y': 3, 'x': 5, 'id': 11},
			                 {'y': 4, 'x': 1, 'id': 12},
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 8), board_removed)

		board_removed = [
			                 {'y': 3, 'x': 4, 'id': 10},
			                 {'y': 3, 'x': 5, 'id': 11},
			                 {'y': 4, 'x': 1, 'id': 12},
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 9), board_removed)

		board_removed = [
			                 {'y': 3, 'x': 5, 'id': 11},
			                 {'y': 4, 'x': 1, 'id': 12},
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 10), board_removed)

		board_removed = [
			                 {'y': 4, 'x': 1, 'id': 12},
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 11), board_removed)

		board_removed = [
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 12), board_removed)

		board_removed = [
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 13), board_removed)

		board_removed = [
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 14), board_removed)

		board_removed = [
			                 {'y': 4, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 15), board_removed)

		board_removed = [
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

		self.assertEqual(removePawn(board, 16), board_removed)

	def test_movePawn(self):
		'''test the move of each pawns.'''
        board = init_board()
		#moved {'y': 4,'x': 5,'id': 16} to {'y': 5,'x': 5,'id': 16}
        board_moved = [
			                 {'y': 1, 'x': 1, 'id': 1},
			                 {'y': 1, 'x': 3, 'id': 2},
			                 {'y': 1, 'x': 5, 'id': 3},
			                 {'y': 2, 'x': 2, 'id': 4},
			                 {'y': 2, 'x': 3, 'id': 5},
			                 {'y': 2, 'x': 4, 'id': 6},
			                 {'y': 3, 'x': 1, 'id': 7},
			                 {'y': 3, 'x': 2, 'id': 8},
			                 {'y': 3, 'x': 3, 'id': 9},
			                 {'y': 3, 'x': 4, 'id': 10},
			                 {'y': 3, 'x': 5, 'id': 11},
			                 {'y': 4, 'x': 1, 'id': 12},
			                 {'y': 4, 'x': 2, 'id': 13},
			                 {'y': 4, 'x': 3, 'id': 14},
			                 {'y': 4, 'x': 4, 'id': 15},
			                 {'y': 5, 'x': 5, 'id': 16},
			                 {'y': 6, 'x': 1, 'id': 17},
			                 {'y': 6, 'x': 2, 'id': 18},
			                 {'y': 6, 'x': 3, 'id': 19},
			                 {'y': 6, 'x': 4, 'id': 20},
			                 {'y': 6, 'x': 5, 'id': 21},
			                 {'y': 7, 'x': 1, 'id': 22},
			                 {'y': 7, 'x': 2, 'id': 23},
			                 {'y': 7, 'x': 3, 'id': 24},
			                 {'y': 7, 'x': 4, 'id': 25},
			                 {'y': 7, 'x': 5, 'id': 26},
			                 {'y': 8, 'x': 2, 'id': 27},
			                 {'y': 8, 'x': 3, 'id': 28},
			                 {'y': 8, 'x': 4, 'id': 29},
			                 {'y': 9, 'x': 1, 'id': 30},
			                 {'y': 9, 'x': 3, 'id': 31},
			                 {'y': 9, 'x': 5, 'id': 32}
			]

        self.assertEqual(movePawn(board, 16, 5, 5), board_moved)

    def test_valid_process_move(self):
        """ testing a valid process move """
        game = Game.objects.create(player1=self.user1, player2=self.user2)
        game.board_data = init_board()
        game.save()
        
        is_valid, board, message, pawns_removed = processMove(game, self.user2, 1, 6, 1, 5)
        
        self.assertTrue(is_valid)
        self.assertEqual(message, "")
        self.assertEqual(pawns_removed, 0)
        
        game.board_data = board
        game.save()

        is_valid, board, message, pawns_removed = processMove(game, self.user1, 1, 4, 1, 6)
        
        self.assertTrue(is_valid)
        self.assertEqual(message, "")
        self.assertEqual(pawns_removed, 1)

    def test_invalid_process_move(self):
        """ testing an invalid process move """
        game = Game.objects.create(player1=self.user1, player2=self.user2)
        game.board_data = init_board()
        game.save()
        
        is_valid, board, message, pawns_removed = processMove(game, self.user1, 1, 1, 1, 5)
        
        self.assertFalse(is_valid)
        self.assertEqual(message, "Invalid move")
        self.assertEqual(pawns_removed, 0)

    def test_same_location(self):
        """ testing moving pawn to the same location """
        game = Game.objects.create(player1=self.user1, player2=self.user2)
        game.board_data = init_board()
        game.save()
        
        is_valid, board, message, pawns_removed = processMove(game, self.user1, 1, 1, 1, 1)
        
        self.assertFalse(is_valid)
        self.assertEqual(message, "Invalid move- you moved the pawn to the same location")
        self.assertEqual(pawns_removed, 0)

    def test_move_other_pawn(self):
        """ testing moving pawn another player's pawn """
        game = Game.objects.create(player1=self.user1, player2=self.user2)
        game.board_data = init_board()
        game.save()
        
        is_valid, board, message, pawns_removed = processMove(game, self.user1, 1, 6, 1, 5)
        
        self.assertFalse(is_valid)
        self.assertEqual(message, "The pawn you are trying to move does not belong to you")
        self.assertEqual(pawns_removed, 0)


    def test_occupied_location(self):
        """ testing moving pawn to an occupied location """
        game = Game.objects.create(player1=self.user1, player2=self.user2)
        game.board_data = init_board()
        game.save()
        
        is_valid, board, message, pawns_removed = processMove(game, self.user1, 1, 1, 1, 3)
        
        self.assertFalse(is_valid)
        self.assertEqual(message, "Invalid move- there is a pawn in this location")
        self.assertEqual(pawns_removed, 0)


    def test_valid_process_move_handleMoveRequest(self):
        """ testing a valid process move with handleMoveRequest """
        game = Game.objects.create(player1=self.user1, player2=self.user2)
        game.board_data = init_board()
        game.save()
        
        is_valid, board, message, pawns_removed = handleMoveRequest(game.id, self.user2, 1, 6, 1, 5)
        
        self.assertTrue(is_valid)
        self.assertEqual(message, "")
        self.assertEqual(pawns_removed, 0)
        
        game.board_data = board
        game.save()
        
        is_valid, board, message, pawns_removed = handleMoveRequest(game.id, self.user1, 1, 4, 1, 6)
        
        self.assertTrue(is_valid)
        self.assertEqual(message, "")
        self.assertEqual(pawns_removed, 1)
    
    def test_invalid_process_move_handleMoveRequest(self):
        """ testing an invalid process move with handleMoveRequest """
        game = Game.objects.create(player1=self.user1, player2=self.user2)
        game.board_data = init_board()
        game.save()
        
        is_valid, board, message, pawns_removed = handleMoveRequest(game.id, self.user1, 1, 1, 1, 5)
        
        self.assertFalse(is_valid)
        self.assertEqual(message, "Invalid move")
        self.assertEqual(pawns_removed, 0)

    def test_same_location_handleMoveRequest(self):
        """ testing moving pawn to the same location with handleMoveRequest """
        game = Game.objects.create(player1=self.user1, player2=self.user2)
        game.board_data = init_board()
        game.save()
        
        is_valid, board, message, pawns_removed = handleMoveRequest(game.id, self.user1, 1, 1, 1, 1)
        
        self.assertFalse(is_valid)
        self.assertEqual(message, "Invalid move- you moved the pawn to the same location")
        self.assertEqual(pawns_removed, 0)
    
    def test_move_other_pawn_handleMoveRequest(self):
        """ testing moving pawn another player's pawn with handleMoveRequest """
        game = Game.objects.create(player1=self.user1, player2=self.user2)
        game.board_data = init_board()
        game.save()
        
        is_valid, board, message, pawns_removed = handleMoveRequest(game.id, self.user1, 1, 6, 1, 5)
        
        self.assertFalse(is_valid)
        self.assertEqual(message, "The pawn you are trying to move does not belong to you")
        self.assertEqual(pawns_removed, 0)


    def test_occupied_location_handleMoveRequest(self):
        """ testing moving pawn to an occupied location with handleMoveRequest """
        game = Game.objects.create(player1=self.user1, player2=self.user2)
        game.board_data = init_board()
        game.save()
        
        is_valid, board, message, pawns_removed = handleMoveRequest(game.id, self.user1, 1, 1, 1, 3)
        
        self.assertFalse(is_valid)
        self.assertEqual(message, "Invalid move- there is a pawn in this location")
        self.assertEqual(pawns_removed, 0)


class ViewsTest(TestCase):
    """ Testing Views """
    def setUp(self):
        """ setup """
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='John', email='john@abc.com')

    def test_index(self):
        """ testing index """
        # Create an instance of a GET request
        request = self.factory.get('/')

        # simulate an anonymous user
        request.user = AnonymousUser()

        # Test view
        response = views.index(request)
        self.assertEqual(response.status_code, 200)


    def test_leaderboard(self):
        """ testing leaderboard """
        # Create an instance of a GET request
        request = self.factory.get('/leaderboard')

        # simulate a logged-in user by setting request.user .
        request.user = self.user

        # Test view
        response = views.LeaderboardView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_new_game(self):
        """ testing new game """
        # Create an instance of a GET request
        request = self.factory.get('/new-game')

        # simulate a logged-in user by setting request.user .
        request.user = self.user

        # Test view
        response = views.new_game(request)
        self.assertEqual(response.status_code, 302)

        game = Game.objects.get(id=1)
        self.assertEqual(game.player1, self.user)

        user2 = User.objects.create_user(username='Jane', email='jane@abc.com')

        # second user creates new game
        request = self.factory.get('/new-game')

        # simulate a logged-in user by setting request.user .
        request.user = user2

        # Test view
        response = views.new_game(request)
        self.assertEqual(response.status_code, 302)

        game = Game.objects.get(id=1)
        self.assertEqual(game.player2, user2)

    def test_rules(self):
        """ testing rules """
        # Create an instance of a GET request
        request = self.factory.get('/rules')

        # simulate an anonymous user
        request.user = AnonymousUser()

        # Test view
        response = views.rules(request)
        self.assertEqual(response.status_code, 200)

    def test_signup(self):
        """ testing signup """
        # Create an instance of a GET request
        request = self.factory.get('/signup')

        # simulate a logged-in user by setting request.user .
        request.user = self.user


        # Test view
        response = views.SignupView.as_view()(request)
        self.assertEqual(response.status_code, 200)


    def test_login(self):
        """ testing login """
        # Create an instance of a GET request
        request = self.factory.get('/login')

        # simulate a logged-in user by setting request.user .
        request.user = self.user


        # Test view
        response = views.LoginView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_game(self):
        """ testing  game page """
        # Create an instance of a GET request
        request = self.factory.get('/game/1')

        # simulate a logged-in user by setting request.user .
        request.user = self.user

        Game.objects.create(player1=request.user)
        # Test view
        response = views.GameDetailView.as_view()(request, pk=1)
        self.assertEqual(response.status_code, 200)

    def test_game_list(self):
        """ testing game list """
        # Create an instance of a GET request
        request = self.factory.get('/my-games')

        # simulate a logged-in user by setting request.user .
        request.user = self.user

    
        # Test view
        response = views.UserGamesView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_credits(self):
        """ testing credits """
        # Create an instance of a GET request
        request = self.factory.get('/credits')

        # simulate a logged-in user by setting request.user .
        request.user = self.user

        # Test view
        response = views.game_credits(request)
        self.assertEqual(response.status_code, 200)

    def test_game_json(self):
        """ testing game json """
        Game.objects.create(player1=self.user)
        # Create an instance of a GET request
        request = self.factory.get('/game/1/json')

        # simulate a logged-in user by setting request.user .
        request.user = self.user

        # Test view
        response = views.get_game_json(request, pk=1)
        self.assertEqual(response.status_code, 200)


    def test_invite(self):
        """ testing invite """
        # Create an instance of a GET request
        request = self.factory.get('/invite-friend')

        # simulate a logged-in user by setting request.user .
        request.user = self.user

        # Test view
        response = views.invite_friend(request)
        self.assertEqual(response.status_code, 200)

class ModelsTest(TestCase):
    """ Testing Models """
    def setUp(self):
        """ setup """
        self.user1 = User.objects.create_user(
            username='John', first_name="John",
            email='john@abc.com',
            password='pass'
        )
        self.user2 = User.objects.create_user(
            username='Jane', first_name="Jane",
            email='jane@abc.com',
            password='pass'
        )

    def test_game(self):
        """ testing game creation """
        game1 = Game.objects.create(player1=self.user1, player2=self.user2)
        game1.save()

        self.assertEqual(game1.player1, self.user1)
        self.assertEqual(game1.player2, self.user2)
        self.assertEqual(game1.status, "M")

        game2 = Game.objects.get(id=game1.id)
        self.assertEqual(game2.player1, self.user1)
        self.assertEqual(game2.player2, self.user2)


class ConsumersTests(ChannelTestCase):
    """ Testing Consumers/Channels/Sockets """
    def test_group(self):
        """ testing groups """
        # Add a test channel to a test group
        Group("test-group").add("test-channel")
        # Send to the group
        Group("test-group").send({"status": "welcome", "message": "Test"})
        # Verify the message got into the destination channel
        result = self.get_next_message("test-channel", require=True)
        self.assertEqual(result['status'], "welcome")
        self.assertEqual(result['message'], "Test")

    def test_channel(self):
        """ testing channels """
        # Add a message onto the channel to use in a consumer
        Channel("message").send({"status": "welcome", "message": "Test"})
        result = self.get_next_message("message", require=True)
        self.assertEqual(result['status'], "welcome")
        self.assertEqual(result['message'], "Test")

    def test_consumers(self):
        """ testing consumers """
        client = HttpClient()
        user = User.objects.create_user(
            username='john@abc.com', email='john@abc.com', password='pass')
        client.login(username='john@abc.com', password='pass')

        client.send_and_consume('websocket.connect', path='/welcome')
        # check that there is nothing to receive
        self.assertIsNone(client.receive())


        client.send_and_consume('websocket.receive',
                                text={'status': 'welcome', 'message': 'test'},
                                path='/welcome')
        # test 'response'
        result = client.receive()
        self.assertEqual(result["status"], "welcome")

                                        
        # There is nothing to receive
        self.assertIsNone(client.receive())



class LegalMovesTests(ChannelTestCase):
    """ Testing Legal Moves """
    def test_valid_legal_move(self):
        """ testing legal moves """
        self.assertTrue(is_move_legal(1, 1, 1, 3))

    def test_invalid_legal_move(self):
        """ testing legal moves """
        self.assertFalse(is_move_legal(1, 1, 1, 4))

class LegalJumpsTests(ChannelTestCase):
    """ Testing Legal Jumps """
    def test_valid_legal_jump_path(self):
        """ testing legal jumps """
        jump = {
            '15': '13',
            '33': '22',
        }
        self.assertEqual(get_legal_jumps(1, 1), jump)

    def test_invalid_legal_jump_path(self):
        """ testing legal jumps """
        jump = {
            '15': '13',
            '33': '22',
        }
        self.assertNotEqual(get_legal_jumps(1, 1), [])

    def test_legal_jumps_pawns_removed(self):
        """ testing pawns to remove """
        self.assertEqual(legal_jump(1, 1, 1, 5), ['13'])
        self.assertEqual(legal_jump(1, 1, 3, 1), [])
