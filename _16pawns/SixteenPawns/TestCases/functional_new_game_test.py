import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

class NewGameTest(unittest.TestCase):
	
	def setUp(self):
		self.driver = webdriver.Firefox()

	def user_test(self):
		driver = self.driver
		driver.get('https://sixteenpawns.herokuapp.com')
		self.assertIn("Welcome to 16 pawns", driver.title)
		driver.implicitly_wait(10)
		email = driver.find_element_by_id('id_email')
		email.clear()
		email.send_keys('test@citymail.cuny.edu')	
		password = driver.find_element_by_id('id_password')
		password.clear()
		password.send_keys('abc123')	
		driver.find_element_by_id('id_login').click()
		driver.implicitly_wait(10)
		driver.find_element_by_id('id_new_game').click()

	def tearDown(self):
		self.driver.close()

if __name__ == "__main__":
    unittest.main()