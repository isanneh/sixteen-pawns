from django.test import Client
from django.test import TestCase
from django.contrib.auth.models import User
from SixteenPawns.models import Game

class IntegrationTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user( username='jane@abc.com', first_name='Jane', email='jane@abc.com')
        self.user.set_password('pass')
        self.user.save()
                                            
    
    def test_valid_signup(self):
        c = Client()
        response = c.post('/signup', {'first_name': 'John',
                                       'last_name': 'Doe',
                                       'email': 'john@abc.com',
                                       'email_cn': 'john@abc.com',
                                       'password': 'pass',
                                       'password_cn': 'pass'}, follow=True)
                                       
        #print response.context['form']
        
        self.assertFalse(response.context.get('form', False))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "index.html")
    
    def test_invalid_signup(self):
        c = Client()
        response = c.post('/signup', {'first_name': 'John',
                          'last_name': 'Doe',
                          'email': 'john@abc.com',
                          'email_cn': 'john@abc.com',
                          'password': 'pass',
                          'password_cn': 'pass2'}, follow=True)
            
        self.assertTrue(response.context['form'].errors)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "signup.html")

    def test_login(self):
        c = Client()
        response = c.post('/login', {'email': 'jane@abc.com',
                                     'password': 'pass'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context.get('form', False))
        self.assertTemplateUsed(response, "index.html")
        self.assertEqual(str(c.session.get('_auth_user_id', None)), str(self.user.pk))
    

    def test_invalid_login(self):
        c = Client()
        response = c.post('/login', {'email': 'jane@abc.com',
                          'password': 'pass2'}, follow=True)
        self.assertTrue(response.context['form'].errors)
        self.assertTemplateUsed(response, "index.html")


    def test_new_game(self):
        c = Client()
        
        # login first user
        a = c.login(username='jane@abc.com', password='pass')
        
        # create new game
        response = c.get('/new-game', follow=True)
        
        self.assertEqual(response.status_code, 200)
        
        game = Game.objects.last()
        self.assertEqual(game.player1.email, 'jane@abc.com')
        self.assertEqual(game.player1, self.user)
        self.assertEqual(game.status, "M")
        
        #logout
        c.logout()
        
        
        user2 = User.objects.create_user( username='john@abc.com', first_name='John', email='john@abc.com')
        user2.set_password('pass')
        user2.save()
        
        # login first user
        c.login(username='john@abc.com', password='pass')
        
        # create new game
        response = c.get('/new-game', follow=True)
        
        self.assertEqual(response.status_code, 200)
        
        # check if users were matched to the same game
        game = Game.objects.last()
        self.assertEqual(game.player1, self.user)
        self.assertEqual(game.player2, user2)


        self.assertEqual(game.player1_score, 0)
        self.assertEqual(game.player2_score, 0)
        self.assertEqual(game.status, "IP")

    def test_logout(self):
        c = Client()
        c.login(username='janen@abc.com', password='pass')
        response = c.post('/login', {'email': 'jane@abc.com',
                          'password': 'pass'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "index.html")
        self.assertEqual(str(c.session.get('_auth_user_id', None)), str(self.user.pk))
                          
        c.get('/logout')
                          
        #c.logout()
        self.assertNotEqual(str(c.session.get('_auth_user_id', None)), str(self.user.pk))











