import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

class SignUpTest(unittest.TestCase):
	
	def setUp(self):
		self.driver = webdriver.Firefox()

	def test_UserRegistration(self):
		driver = self.driver
		driver.get('https://sixteenpawns.herokuapp.com')
		self.assertIn("Welcome to 16 pawns", driver.title)
		driver.implicitly_wait(10)
		driver.find_element_by_link_text('Join Us').click()
		driver.implicitly_wait(10)		
		
		userInfo = {
			'id_first_name': 'Tester', 
			'id_last_name': 'TestingTheTester', 
			'id_email': 'tester2@citymail.cuny.edu', 
			'id_email_cn': 'tester2@citymail.cuny.edu',
			'id_password': 'abc1234', 
			'id_password_cn': 'abc1234'
		}

		for key, val in userInfo.iteritems():
			driver.find_element_by_id(key).clear()
			driver.find_element_by_id(key).send_keys(val)

		driver.find_element_by_id('id_sign_up').click()


	def tearDown(self):
		self.driver.close()

if __name__ == "__main__":
    unittest.main()