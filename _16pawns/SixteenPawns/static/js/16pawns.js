/* Global variables that can be accessed anywhere */

//var pawns = {}; // dictionary to hold pawns
var user_alert = document.getElementById("user_alert_body");
// board coordinates
var  gameBoardCoordinates =
    [
                [null, null],  [null, null], [null, null], [null, null], [null, null],
                [null, null],  [null, null], [null, null], [null, null], [null, null],
                [null, null],  [null, null], [null, null], [null, null], [null, null],
                [null, null],  [null, null], [null, null], [null, null], [null, null],
                [null, null],  [null, null], [null, null], [null, null], [null, null],
                [null, null],  [null, null], [null, null], [null, null], [null, null],
                [null, null],  [null, null], [null, null], [null, null], [null, null],
                [null, null],  [null, null], [null, null], [null, null], [null, null],
                [null, null],  [null, null], [null, null], [null, null], [null, null],
    ];

function getInitialBoard() {
  //Let's define our board by a matrix
  //Entries that are -1 are locations that cannot be used
  //Entries that are 0 are locations that are empty and can be used
  //Entries that are 1 are initial locations for player 1
  //Entries that are 2 are initial locations for player 2
  var board = 
    [
        [0, -1, 0, -1, 0],
        [-1, 0, 0, 0, -1],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [-1, 0, 0, 0, -1],
        [0, -1, 0, -1, 0]
    ];
  return board;
}

var board = getInitialBoard();

function resetBoard(){
    board =
//Let's define our board by a matrix
//Entries that are -1 are locations that cannot be used
//Entries that are 0 are locations that are empty and can be used
//Entries that are 1 are initial locations for player 1
//Entries that are 2 are initial locations for player 2

[
        [0, -1, 0, -1, 0],
        [-1, 0, 0, 0, -1],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [-1, 0, 0, 0, -1],
        [0, -1, 0, -1, 0]
];

}

var rows = 9;
var cols = 5;
var radius = 10;
var lineWidth = 3;
var player1Color = "red";
var player2Color = "blue";
var previous_position = null; // position of pawn to be moved
var next_position = null;  // location of where pawn should be moved




var refreshBoard = function(){
        //update in db
     $.ajax({
        url: "/game/" + game_id +"/json",
        method: 'GET', // or another (GET), whatever you need,
        success: function (data) {        
            // success callback
            // you can process data returned by function from views.py
            //console.log(data); //just gives you back html... we need json
            board_json = data;
            resetBoard();
            Draw();
        }
    });
 };


/* End of global variables */

function CreateRenderingMatrix(){
    console.log('new board');
    console.log(board_json);
    var len = board_json.length;

    pawns = {};
    board = getInitialBoard();

    for (var i=0; i<len; i++){
        var  pawn = board_json[i];
        var y = parseInt(pawn.y)-1;
        var x = parseInt(pawn.x)-1
        if(pawn.id <= 16){
            //player 1
            board[y][x] = 1;
            var key = y  + "," + x;
            pawns[key] = {
            player: 1,
            id: parseInt(pawn.id),
            color: player1Color
            }
        } else {
            //player 2
            board[y][x] = 2;
            var key = y  + "," + x;
            pawns[key] = {
            player: 2,
            id: parseInt(pawn.id),
            color: player2Color
            }
        }
    }
}

function Draw(){
	var canvas = document.getElementById('canvas');
	var context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
	var xDistance = 80;
	var yDistance = 60;

    CreateRenderingMatrix(); //this will take the board_json and create board matrix

        // initialize positions
	for(var i = 0; i < rows; i++){
	    for(var j = 0; j < cols; j++){
                var offset = radius * 2 + lineWidth;
                var center = radius + lineWidth;
                var x = j * (offset + xDistance) + center;
                var y = i * (offset + yDistance) + center;

                // store board coordinates
                gameBoardCoordinates[i][j] = [x, y];

                var id = i + "," + j;
                if(board[i][j] === 1) {
                    pawns[id].x = x;
                    pawns[id].y = y;
                }
                else if(board[i][j] === 2) {
                    pawns[id].x = x;
                    pawns[id].y = y;
                }
            }
        }

        // draw layout
        drawLayout(context);

        // draw pawns
        drawPawns(context);
}
//@TODO we need to determine legal moves


function drawLayout(ctx) {

    // 4 by 4 grid (horizontal and vertical lines)
    for(var i = 2; i < rows-2; i++){
            // rows
            ctx.beginPath();
            ctx.moveTo(gameBoardCoordinates[i][0][0], gameBoardCoordinates[i][0][1]);
            ctx.lineTo(gameBoardCoordinates[i][cols-1][0], gameBoardCoordinates[i][cols-1][1]);
            ctx.closePath();
            ctx.stroke();

            // cols
            ctx.beginPath();
            ctx.moveTo(gameBoardCoordinates[2][i-2][0], gameBoardCoordinates[2][i-2][1]);
            ctx.lineTo(gameBoardCoordinates[rows-3][i-2][0], gameBoardCoordinates[rows-3][i-2][1]);
            ctx.closePath();
            ctx.stroke();
    }

    // diagonal lines

    var j = 0;
    var k = 4;
    for(var i = 0; i < 3; i++) {
        ctx.beginPath();
        ctx.moveTo(gameBoardCoordinates[j][0][0], gameBoardCoordinates[j][0][1]);
        ctx.lineTo(gameBoardCoordinates[k][4][0], gameBoardCoordinates[k][4][1]);
        ctx.closePath();
        ctx.stroke();


        ctx.beginPath();
        ctx.moveTo(gameBoardCoordinates[j][4][0], gameBoardCoordinates[j][4][1]);
        ctx.lineTo(gameBoardCoordinates[k][0][0], gameBoardCoordinates[k][0][1]);
        ctx.closePath();
        ctx.stroke();

        j = j + 2;
        k = k + 2;
    }


   // outer top horizontal lines

   ctx.beginPath();
   ctx.moveTo(gameBoardCoordinates[0][0][0], gameBoardCoordinates[0][0][1]);
   ctx.lineTo(gameBoardCoordinates[0][cols-1][0], gameBoardCoordinates[0][cols-1][1]);
   ctx.closePath();
   ctx.stroke();

   ctx.beginPath();
   ctx.moveTo(gameBoardCoordinates[1][1][0], gameBoardCoordinates[1][1][1]);
   ctx.lineTo(gameBoardCoordinates[1][3][0], gameBoardCoordinates[1][3][1]);
   ctx.closePath();
   ctx.stroke();

   // outer bottom horizontal lines

   ctx.beginPath();
   ctx.moveTo(gameBoardCoordinates[rows-1][0][0], gameBoardCoordinates[rows-1][0][1]);
   ctx.lineTo(gameBoardCoordinates[rows-1][cols-1][0], gameBoardCoordinates[rows-1][cols-1][1]);
   ctx.closePath();
   ctx.stroke();

   ctx.beginPath();
   ctx.moveTo(gameBoardCoordinates[rows-2][1][0], gameBoardCoordinates[rows-2][1][1]);
   ctx.lineTo(gameBoardCoordinates[rows-2][3][0], gameBoardCoordinates[rows-2][3][1]);
   ctx.closePath();
   ctx.stroke();

   // center vertical line

   ctx.beginPath();
   ctx.moveTo(gameBoardCoordinates[0][2][0], gameBoardCoordinates[0][2][1]);
   ctx.lineTo(gameBoardCoordinates[rows-1][2][0], gameBoardCoordinates[rows-1][2][1]);
   ctx.closePath();
   ctx.stroke();
}

function drawPawns(context) {
    for (var key in pawns) {
        context.beginPath();
	context.arc(pawns[key].x, pawns[key].y, radius, 0, 2 * Math.PI, false);
	context.fillStyle = pawns[key].color;
	context.fill();
	context.lineWidth = lineWidth;
	context.strokeStyle = '#003300';
        context.stroke()

    }
}

function highlightPawn(ctx, x, y, color) {
    console.log('highlight ' + x + ' ' + y);
    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.arc(x,y,radius,0,2*Math.PI);
    ctx.fill();
}

function nextMove(context) {
    var x1 = previous_position.col;
    var y1 = previous_position.row;
    var x2 = next_position.col;
    var y2 = next_position.row;
    
    var key = y1 + "," + x1;

    if(pawns[key].player === 1 && player_num === 2) {
        highlightPawn(context, gameBoardCoordinates[y1][x1][0], gameBoardCoordinates[y1][x1][1], player1Color);
        user_alert.innerHTML = "The pawn you are trying to move does not belong to you.";
        $('#user_alert').modal('show');
    }
    else if(pawns[key].player === 2 && player_num === 1) {
        highlightPawn(context, gameBoardCoordinates[y1][x1][0], gameBoardCoordinates[y1][x1][1], player2Color);
        user_alert.innerHTML = "The pawn you are trying to move does not belong to you.";
        $('#user_alert').modal('show');
    }
    
     else if(x1 === x2 && y1 === y2 && player_num === 1) {
        highlightPawn(context, gameBoardCoordinates[y1][x1][0], gameBoardCoordinates[y1][x1][1], pawns[key].color);
    }
    
    else if(x1 === x2 && y1 === y2 && player_num === 2) {
        highlightPawn(context, gameBoardCoordinates[y1][x1][0], gameBoardCoordinates[y1][x1][1], player2Color, pawns[key].color);
    }
    
    else if(player_num === 1 && player1_id  !== current_player_id) {  
        highlightPawn(context, gameBoardCoordinates[y1][x1][0], gameBoardCoordinates[y1][x1][1], pawns[key].color);
        user_alert.innerHTML = "It is not your turn to make a move.";
        $('#user_alert').modal('show');
    }
    else if(player_num === 2 && player2_id  !== current_player_id) { console.log('tttttt' + ' ' + player_num + ' ' + player2_id + ' ' + current_player_id);
        highlightPawn(context, gameBoardCoordinates[y1][x1][0], gameBoardCoordinates[y1][x1][1], pawns[key].color);
        user_alert.innerHTML = "It is not your turn to make a move.";
        $('#user_alert').modal('show');
    }
    else {
    
      data = {"game_id": game_id,
            old_x: x1+1,
            old_y: y1+1,
            new_x: x2+1,
            new_y: y2+1,
            player: player_num,
            status: status,
            state: "move"
           }
      socket.send(JSON.stringify(data));
   
      //update in db
      /* $.ajax({
        url: "/process_move",
        method: 'GET', // or another (GET), whatever you need
        data: {
            gameId: game_id, // data you need to pass to your function
            userId: user_id,
            playerNum: player_num,
            old_x: previous_position.col,
            old_y: previous_position.row,
            new_x: next_position.col,
            new_y: next_position.row
        },
        success: function (data) {        
            // success callback
            // you can process data returned by function from views.py
            console.log(data);
        }
      });*/

    }

    previous_position = null;
    next_position = null;
}

$('#canvas').click(function (event) {
    /* this function is triggered upon a click event
       to track board/pawn postion clicked */
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    var clickedX = event.pageX - this.offsetLeft;
    var clickedY = event.pageY - this.offsetTop;


    var rect = canvas.getBoundingClientRect();
    var clickedX = event.clientX - rect.left;
    var clickedY = event.clientY - rect.top;


    //console.log(clickedX + "," + clickedY);
    //console.log(gameBoardCoordinates[0][0][0]);
    //console.log(gameBoardCoordinates[0][0][1]);
    for (var i = 0; i < rows; i++) {
        for(var j = 0; j < cols; j++) {
            var x = gameBoardCoordinates[i][j][0];
            var y = gameBoardCoordinates[i][j][1];
            var left =  x - radius;
            var right = x + radius;
            var top = y - radius;
            var bottom = y + radius;
            //console.log(" x:"+x + " y:"+y + " L:"+left + " R:"+right +  " top:"+top  + " bottom:"+bottom );
            if (clickedX < right && clickedX > left && clickedY > top && clickedY < bottom) {
                // check if clicked location is within any the bounding box 
                // of any of the board coordinates 
                if (previous_position === null) {
                    previous_position = {
                        position: gameBoardCoordinates[i][j],
                        row: i,
                        col: j,
                    };

                    var key = i + "," + j;

                    if(pawns[key] === undefined) {
                        user_alert.innerHTML = "There is no pawn in this location";
                        $('#user_alert').modal('show');
                        previous_position = null;
                    }
                    else {
                        highlightPawn(context, gameBoardCoordinates[i][j][0], gameBoardCoordinates[i][j][1], "yellow");
                    }
                }
                else if (previous_position !== next_position) {
                    next_position = {
                        position: gameBoardCoordinates[i][j],
                        row: i,
                        col: j,
                    };
                    nextMove(context);

                }
            }
        }
    }
});
