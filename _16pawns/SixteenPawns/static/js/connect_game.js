// socket
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
var player_turn = 0;
var alert = document.getElementById("alert");
var player1_turn = document.getElementById("player1_turn");
var player2_turn = document.getElementById("player2_turn");
var player1_score = document.getElementById("player1_score");
var player2_score = document.getElementById("player2_score");
var user_alert = document.getElementById("user_alert_body");
// socket communication
// Note that the path doesn't matter for routing; any WebSocket
// connection gets bumped over to WebSocket consumers

if (window.location.protocol == "https:") {
  var ws_scheme = "wss://";
} else {
  var ws_scheme = "ws://"
};


socket = new WebSocket(ws_scheme + window.location.host + "/game/" + game_id);
socket.onmessage = function(e) {
    console.log('h');
    response_data = JSON.parse(e.data);
    if(response_data.alert_all) {
      alert.innerHTML = response_data.alert_all;
      console.log(response_data.alert_all);
    }
    if(response_data.state === "join") {
      if(response_data.player === 1) {
        var player1 = document.getElementById("player1");
        player1.innerHTML = response_data.user;
      }
      else {
        var player2 = document.getElementById("player2");
        player2.innerHTML = response_data.user;
      }
    }
    else if(response_data.state === "move") {
        console.log(response_data.is_move_legal);
        if(response_data.is_move_legal) {
          current_player_id = response_data.current_player_id;
          board_json = response_data.board;
          Draw();
          player1_score.innerHTML = response_data.player1_score;
          player2_score.innerHTML = response_data.player2_score;
        }
    }
    console.log(response_data.alert_user);
    if(response_data.alert_user) {

      var j = data["old_x"] - 1;
      var i = data["old_y"] - 1;
      var key = i + "," + j;
      highlightPawn(context, gameBoardCoordinates[i][j][0], gameBoardCoordinates[i][j][1], pawns[key].color);
      user_alert.innerHTML = response_data.alert_user;
      $('#user_alert').modal('show');
    }

}

socket.onopen = function() {
    data = {"game_id": game_id,
            old_x: 1,
            old_y: 1,
            new_x: 1,
            new_y: 1,
            player: player_num,
            status: status,
            state: "join",
            player1_score: player1_score,
            player2_score: player2_score
           }
    socket.send(JSON.stringify(data));
}
// Call onopen directly if socket is already open
if (socket.readyState == WebSocket.OPEN) socket.onopen();

function current_turn(player) {
  if(player === 1) {
    player1_turn.innerHTML = "Your turn to make a move";
  }
  if(player === 2) {
    player2_turn.innerHTML = "Your turn to make a move";
  }  
}
