""" forms.py """
from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

class SignupForm(ModelForm):
    """ Signup Form """
    class Meta:
        """ meta """
        model = User
        fields = ['first_name', 'last_name', 'email', 'password']

    email_cn = forms.CharField(label='email_cn', max_length=100)
    password_cn = forms.CharField(label='password_cn', max_length=100)


    def clean(self):
        email = self.cleaned_data.get("email")
        password = self.cleaned_data.get("password")
        password_cn = self.cleaned_data.get("password_cn")

        if password != password_cn:
            error = ("Both passwords don't match")
            self.add_error('password', error)
        if User.objects.filter(email=email).exists():
            error = ("Email Address already exists!")
            self.add_error('email', error)

    def create_user(self):
        """ create new user """
        first_name = self.cleaned_data.get("first_name")
        last_name = self.cleaned_data.get("last_name")
        email = self.cleaned_data.get("email")
        password = self.cleaned_data.get("password")
        user = User.objects.create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            username=email)

        user.set_password(password)
        user.save()

class LoginForm(ModelForm):
    """ login form """
    class Meta:
        model = User
        fields = ['email', 'password']

    def clean(self):
        # authenticate user
        email = self.cleaned_data.get("email")
        password = self.cleaned_data.get("password")
        user = authenticate(username=email, password=password)
        self.user = user
        if user is None:
            # authentication failed: add error message
            error = 'Invalid email/password'
            self.add_error('email', error)

    def get_user(self):
        """ get user """
        return self.user

    def authenticate_user(self, request):
        """ authenticate user """
        # authenticate user
        email = self.cleaned_data.get("email")
        password = self.cleaned_data.get("password")
        user = authenticate(username=email, password=password)
        if user is None:
            # authentication failed: add error message
            error = 'Invalid email/password'
            self.add_error('email', error)
