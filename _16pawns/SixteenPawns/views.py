# -*- coding: utf-8 -*-
""" views.py """
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import JsonResponse
from django.views.generic.edit import FormView
from django.views.generic.base import RedirectView
from django.contrib.auth import login, logout
from django.contrib import messages
from django.shortcuts import redirect
from django.views.generic.detail import DetailView
from django.db.models import Q, Count
from django.views.generic.list import ListView
from SixteenPawns.game_engine import handleMoveRequest, init_board
from SixteenPawns.forms import SignupForm, LoginForm
from SixteenPawns.models import Game


# Create your views here.

def index(request):
    '''creates request'''
    return render(request, 'index.html', context={})

def new_game(request):
    '''check if user is currently playing a game or not.'''
    earliest_game = Game.objects.filter(status='M').exclude(
        player1=request.user).first()
    if earliest_game is not None:
        # user joins an existing game
        # user is matched with the player that is first in the queue
        earliest_game.player2 = request.user
        earliest_game.status = "IP"
        earliest_game.save()
        game_id = earliest_game.id
    else:
        # create a new game
        board = init_board()
        game = Game.objects.create(
            player1=request.user,
            board_data=board,
            current_player=request.user,
        )
        game_id = game.id
    return redirect('game', pk=game_id)

def process_move(request):
    '''precess the valid move, if not decline it.'''
    game_id = request.GET["gameId"]
    user_id = request.GET["userId"]
    old_x = int(request.GET["old_x"]) + 1
    old_y = int(request.GET["old_y"]) + 1
    new_x = int(request.GET["new_x"]) + 1
    new_y = int(request.GET["new_y"]) + 1
    if handleMoveRequest(game_id, user_id, old_x, old_y, new_x, new_y):
        print "Valid Move"
    else:
        print "Not Valid"
    return redirect('game', pk=game_id)


def rules(request):
    '''show the rules page'''
    return render(request, 'rules.html', context={})

def usermanual(request):
    '''show the rules page'''
    return render(request, 'usermanual.html', context={})

class LogoutView(RedirectView):
    """ Logout """
    url = '/'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)

class SignupView(FormView):
    """ Sign up """
    template_name = 'signup.html'
    form_class = SignupForm
    success_url = '/'

    def form_valid(self, form):
        '''this method is called when a valid form of data has
            been posted, it would return an http request
        '''
        form.create_user()
        #print "valid"
        messages.success(self.request, 'Your account has been created.')
        return super(SignupView, self).form_valid(form)

    def form_invalid(self, form):
        '''shows invalid move sign'''
        #print "invalid"
        return super(SignupView, self).form_invalid(form)

class LoginView(FormView):
    """ Login """
    template_name = 'index.html'
    form_class = LoginForm
    success_url = '/'

    def form_valid(self, form):
        '''this method is called when a valid form of data has
            been posted, it would return an http request
            get from form.authenticate_user
        '''
        login(self.request, form.get_user())
        return super(LoginView, self).form_valid(form)

def get_game_json(request, pk):
    '''get's the json data, and prints game_id'''
    game_id = pk
    print game_id
    game = Game.objects.get(pk=pk).board_data
    print "GAME", game
    return JsonResponse(game, safe=False)


class GameDetailView(DetailView):
    """ Game Detail View """
    model = Game
    template_name = 'game.html'

    def get_context_data(self, **kwargs):
        '''gets the context data, calls self.request.user, other wise return to the context'''
        pk = self.kwargs['pk']
        game = Game.objects.get(pk=pk)
        context = super(GameDetailView, self).get_context_data(**kwargs)
        context['board'] = game.board_data
        player_num = 2
        if game.player1 == self.request.user:
            player_num = 1
        context['player_num'] = player_num
        return context

def invite_friend(request):
    '''goto the invite friend page'''
    return render(request, 'invite.html', context={})

def game_credits(request):
    '''go to the credit page'''
    return render(request, 'credit.html', context={})

class UserGamesView(ListView):
    '''creates a game view from the client side'''

    model = Game
    template_name = 'game_list.html'

    def get_queryset(self):
        query = Game.objects.filter(
            Q(player1=self.request.user) |
            Q(player2=self.request.user)
        ).all()
        return query

class LeaderboardView(ListView):
    '''creates the leaderboard views'''

    model = Game
    template_name = 'leaderboard.html'

    def get_queryset(self):
        '''calls game.objects.annotate, shows who winned the game'''
        query = Game.objects.annotate(
            num_wins=Count('winner')).order_by('-num_wins').exclude(winner__isnull=True)[:10]
        return query
