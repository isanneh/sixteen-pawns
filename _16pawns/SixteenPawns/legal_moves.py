""" legal moves """
#x1=1
#y1=1
#x2=3
#y2=1
#is_move_legal(y1,x1,y2,x2)
#is_move_legal(1,1,2,2)
#true
#is_move_legal(1,1,2,5)
#false



#y come first, x comes second


def get_legal_moves():
    ''''gets the legal moving coordinate to start the moves. '''
    legal_moves = {}
    #first row
    legal_moves['11'] = ['13', '22']
    legal_moves['13'] = ['23', '11', '15']
    legal_moves['15'] = ['13', '24']
    #second row
    legal_moves['22'] = ['11', '23', '33']
    legal_moves['23'] = ['13', '33', '22', '24']
    legal_moves['24'] = ['15', '23', '33']
    #third row
    legal_moves['31'] = ['32', '41', '42']
    legal_moves['32'] = ['31', '42', '33']
    legal_moves['33'] = ['22', '23', '24', '32', '34', '42', '43', '44']
    legal_moves['34'] = ['33', '44', '35']
    legal_moves['35'] = ['34', '44', '45']
    #fourth row
    legal_moves['41'] = ['31', '51', '42']
    legal_moves['42'] = ['31', '32', '33', '41', '43', '51', '52', '53']
    legal_moves['43'] = ['33', '42', '44', '53']
    legal_moves['44'] = ['33', '34', '35', '43', '45', '53', '54', '55']
    legal_moves['45'] = ['35', '44', '55']
    #fifth row
    legal_moves['51'] = ['41', '42', '52', '62', '61']
    legal_moves['52'] = ['42', '53', '62', '51']
    legal_moves['53'] = ['42', '43', '44', '52', '54', '62', '63', '64']
    legal_moves['54'] = ['44', '53', '55', '64']
    legal_moves['55'] = ['45', '44', '54', '64', '65']
    #sixth row
    legal_moves['61'] = ['51', '71', '62']
    legal_moves['62'] = ['51', '52', '53', '61', '63', '71', '72', '73']
    legal_moves['63'] = ['53', '62', '64', '73']
    legal_moves['64'] = ['53', '54', '55', '63', '65', '73', '74', '75']
    legal_moves['65'] = ['55', '64', '75']
    #seventh row
    legal_moves['71'] = ['62', '61', '72']
    legal_moves['72'] = ['71', '62', '73']
    legal_moves['73'] = ['62', '63', '64', '72', '74', '82', '83', '84']
    legal_moves['74'] = ['73', '64', '75']
    legal_moves['75'] = ['74', '64', '65']
    #eigth row
    legal_moves['82'] = ['91', '83', '73']
    legal_moves['83'] = ['93', '73', '82', '84']
    legal_moves['84'] = ['95', '83', '73']
    #ninth row
    legal_moves['91'] = ['93', '82']
    legal_moves['93'] = ['83', '91', '95']
    legal_moves['95'] = ['93', '84']
    return legal_moves



def is_move_legal(y1, x1, y2, x2):
    '''checks if the move is legal or not'''
    previous_position = str(y1) + str(x1)
    new_position = str(y2) + str(x2)
    legal_moves_dict = get_legal_moves()
    legal_moves = legal_moves_dict[previous_position]
    if new_position in legal_moves:
        return True
    return False
'''
#board
#   1     2    3   4    5

#1  1,1       1,3      1,5

#2       2,2  2,3  2,4

#3  3,1  3,2  3,3  3,4  3,5

#4  4,1  4,2  4,3  4,4  4,5

#5  5,1  5,2  5,3  5,4  5,5

#6  6,1  6,2  6,3  6,4  6,5

#7  7,1  7,2  7,3  7,4  7,5

#8       8,2  8,3  8,4

#9  9,1       9,3       9,5
'''
