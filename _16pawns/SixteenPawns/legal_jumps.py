'''design of the board'''
#   1     2    3   4    5

#1  1,1       1,3      1,5

#2       2,2  2,3  2,4

#3  3,1  3,2  3,3  3,4  3,5

#4  4,1  4,2  4,3  4,4  4,5

#5  5,1  5,2  5,3  5,4  5,5

#6  6,1  6,2  6,3  6,4  6,5

#7  7,1  7,2  7,3  7,4  7,5

#8       8,2  8,3  8,4

#9  9,1       9,3       9,5



#x1=1
#y1=1
#x2=3
#y2=1
#is_move_legal(y1,x1,y2,x2)
#is_move_legal(1,1,2,2)
#true
#is_move_legal(1,1,2,5)
#false



#y come first, x comes second


print" The board with all place for pawns"
print"1,1       1,3      1,5"
print"     2,2  2,3  2,4"
print"3,1  3,2  3,3  3,4  3,5"
print"4,1  4,2  4,3  4,4  4,5"
print"5,1  5,2  5,3  5,4  5,5"
print"6,1  6,2  6,3  6,4  6,5"
print"7,1  7,2  7,3  7,4  7,5"
print"     8,2  8,3  8,4"
print"9,1       9,3       9,5"


def get_legal_jumps(y1, x1):
    '''start of the legal jumps moves'''

    legal_jumps = {}
    
    #first row
    legal_jumps['11'] = {
        '15': '13', # 11 to 15 removes 13
        '33': '22', # 11 to 33 removes 22
    }

    legal_jumps['13'] = {
        '33': '23'
    }

    legal_jumps['15'] = {
        '11': '13',
        '33': '24'
    }

    #second row
    legal_jumps['22'] = {
        '24': '23',
        '44': '33'
    }

    legal_jumps['23'] = {
        '43': '33'
    }
    
    legal_jumps['24'] = {
        '22': '23',
        '42': '33'
    }

    #third row
    legal_jumps['31'] = {
        '33': '32',
        '53': '42',
        '51': '41',
    }

    legal_jumps['32'] = {
        '34': '33',
        '52': '42'
    }

    legal_jumps['33'] = {
        '31': '32',
        '35': '34',
        '51': '42',
        '53': '43',
        '55': '44',
        '11': '22',
        '15': '24',
        '13': '23'
    }

    legal_jumps['34'] = {
        '32': '33',
        '54': '44'
    }

    legal_jumps['35'] = {
        '33': '34',
        '53': '44',
        '55': '45',
    }

    #fourth row

    legal_jumps['41'] = {
        '61': '51',
        '43': '42'
    }

    legal_jumps['42'] = {
        '24': '33',
        '44': '43',
        '62': '52',
        '64': '53'
    }
    
    legal_jumps['43'] = {
        '23': '33',
        '41': '42',
        '45': '44',
        '63': '53'
    }
    
    legal_jumps['44'] = {
        '22': '33',
        '42': '43',
        '62': '53',
        '64': '54'
    }
    
    legal_jumps['45'] = {
        '43': '44',
        '65': '55'
    }

    #fifth row


    legal_jumps['51'] = {
        '31': '41',
        '33': '42',
        '53': '52',
        '73': '62',
        '71': '61'
    }
    
    legal_jumps['52'] = {
        '32': '42',
        '54': '53',
        '72': '62',
    }
    
    legal_jumps['53'] = {
        '31': '42',
        '33': '43',
        '35': '44',
        '51': '52',
        '55': '54',
        '71': '62',
        '73': '63',
        '75': '64'
    }
    
    legal_jumps['54'] = {
        '34': '44',
        '52': '53',
        '74': '64'
    }

    legal_jumps['55'] = {
        '35': '45',
        '33': '44',
        '53': '54',
        '73': '64',
        '75': '65'
    }

    # sixth row

    legal_jumps['61'] = {
        '41': '51',
        '63': '62'
    }
    
    legal_jumps['62'] = {
        '42': '52',
        '44': '53',
        '64': '63',
        '84': '73'
    }
    
    legal_jumps['63'] = {
        '43': '53',
        '61': '62',
        '65': '64',
        '83': '73'
                         
    }
    
    legal_jumps['64'] = {
        '42': '53',
        '44': '54',
        '62': '63',
        '82': '73'
    }

    legal_jumps['65'] = {
        '45': '55',
        '63': '64'
    }


    # seventh row

    legal_jumps['71'] = {
        '53': '62',
        '51': '61',
        '73': '72'
    }

    legal_jumps['72'] = [
        ['52', '62'],
        ['74', '73'],
    ]
        
    legal_jumps['73'] = {
        '51': '62',
        '53': '63',
        '55': '64',
        '71': '72',
        '75': '74',
        '91': '82',
        '93': '83',
        '95': '84',
    }
    
    legal_jumps['74'] = {
        '72': '73',
        '54': '64',
    }

    legal_jumps['75'] = {
        '73': '74',
        '53': '64',
        '55': '65'
    }


    #eighth row
    legal_jumps['82'] = {
        '84': '83',
        '64': '73',
    }

    legal_jumps['83'] = {
        '63': '73'
    }

    legal_jumps['84'] = {
        '82': '83',
        '62': '73'
    }

    #ninth row
    legal_jumps['91'] = {
        '95': '93',
        '73': '82'
    }

    legal_jumps['93'] = {
        '73': '83'
    }

    legal_jumps['95'] = {
        '91': '93',
        '73': '84'
    }


    key = str(y1) + str(x1)
    try:
        path = legal_jumps[key]
    except KeyError:
        path = []
    return path


def legal_jump(y1, x1, y2, x2):
    '''taking the legal jumps to old corordinate positions to new coordinate position.'''
    
    # get legal_jumps dict for previous position
    legal_jump_dict = get_legal_jumps(y1, x1)
    
    new_position = str(y2) + str(x2)
    
    try:
        legal_jump = legal_jump_dict[new_position]
        return [legal_jump]
    except KeyError:
        return []
