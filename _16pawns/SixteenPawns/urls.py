'''validation file for every possible urls'''
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^game/(?P<pk>\d+)$', views.GameDetailView.as_view(), name='game'),
    url(r'^game/(?P<pk>\d+)/json', views.get_game_json, name='gameJson'),
    url(r'^rules$', views.rules, name='rules'),
    url(r'^usermanual$', views.usermanual, name='usermanual'),
    url(r'^signup$', views.SignupView.as_view(), name='signup'),
    url(r'^login$', views.LoginView.as_view(), name='login'),
    url(r'^logout$', views.LogoutView.as_view(), name='logout'),
    url(r'^new-game$', views.new_game, name='new_game'),
    url(r'^process_move$', views.process_move, name='process_move'),
    url(r'^invite-friend$', views.invite_friend, name='invite_friend'),
    url(r'^credits$', views.game_credits, name='credits'),
    url(r'^my-games$', views.UserGamesView.as_view(), name='game_list'),
    url(r'^leaderboard$', views.LeaderboardView.as_view(), name='leaderboard'),
]
