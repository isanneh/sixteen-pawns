# -*- coding: utf-8 -*-

""" admin.py """
from __future__ import unicode_literals

from django.contrib import admin
from SixteenPawns.models import Game
# Register your models here.

admin.site.register(Game)
